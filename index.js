import lordiComponent from './src/components/NuxtLordIcon.vue'
import lordIconsObj from './src/lib/lord-icons'

export const NuxtLordIcon = lordiComponent

export const getLordiSrc = (name) => {
  return lordIconsObj[name]?.src || ''
}
export const lordIcons = lordIconsObj
