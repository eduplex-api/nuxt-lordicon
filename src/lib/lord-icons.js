import forEach from 'lodash/forEach'

import lordIconNames from './lord-icon-names'

const baseUrl = 'https://dot0z5gt50bw8.cloudfront.net/s/lordi/'

const formatName = (name) => {
    const regex = /^[0-9-]*|-outline|-morph|.json$/gi
    return name.replace(regex, '')
}

const buildObjet = () => {
    const objNames = {}
    forEach(lordIconNames.names, (name) => {
        objNames[formatName(name)] = {
            src: baseUrl + name
        }
    })
    return objNames
}

const toRet = buildObjet()

export default {
    ...toRet
}

