# Nuxt lordIcon

Custom frontend component to display [LordIcons](https://lordicon.com/)

## License
The source code for the site is licensed under the [**MIT license**](https://gitlab.com/eduplex-api), which you can find in the [LICENSE](../LICENSE/) file.
